package unit_tests

import (
	"gitlab.com/atrico/channels"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/testing/v2/random"
	"testing"
	"time"
)

var rand = random.NewValueGenerator().
	SetDefaultSliceLength(10)

func assertChannel[T any](t *testing.T, ch channels.ReadChannel[T], expected []T) {
	idx := 0
	var val T
	ok := true
	for ok {
		select {
		case val, ok = <-ch:
			if ok {
				assert.That(t, val, is.EqualTo(expected[idx]), "item %d", idx)
				idx++
			}
		case <-time.After(time.Millisecond):
			assert.Fail(t, "Read timeout")
		}
	}
	// Check full length read
	assert.That(t, idx, is.EqualTo(len(expected)), "all values")
}

func createSlice[T any](t *testing.T) (slice []T) {
	if err := rand.Value(&slice); err != nil {
		assert.Fail(t, "failed to create slice: %s", err.Error())
	}
	return
}
