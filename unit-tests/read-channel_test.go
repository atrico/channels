package unit_tests

import (
	"gitlab.com/atrico/channels"
	"testing"
)

func Test_ReadChannel_CreateFromSlice(t *testing.T) {
	// Arrange
	slice := createSlice[int](t)

	// Act
	ch := channels.FromSlice(slice)

	// Assert
	assertChannel(t, ch, slice)
}

func Test_ReadChannel_CreateFromSliceNil(t *testing.T) {
	// Arrange
	var slice []int

	// Act
	ch := channels.FromSlice(slice)

	// Assert
	assertChannel(t, ch, slice)
}

func Test_ReadChannel_SelectSimple(t *testing.T) {
	// Arrange
	slice := createSlice[int](t)
	ch := channels.FromSlice(slice)
	selector := func(v int) int { return v / 2 }
	expected := make([]int, len(slice))
	for i, v := range slice {
		expected[i] = selector(v)
	}

	// Act
	ch2 := ch.Select(selector)

	// Assert
	assertChannel(t, ch2, expected)
}

func Test_ReadChannel_Filter(t *testing.T) {
	// Arrange
	slice := createSlice[int](t)
	ch := channels.FromSlice(slice)
	filter := func(v int) bool { return (v & 1) > 0 }
	expected := make([]int, 0, len(slice))
	for _, v := range slice {
		if filter(v) {
			expected = append(expected, v)
		}
	}

	// Act
	ch2 := ch.Filter(filter)

	// Assert
	assertChannel(t, ch2, expected)
}

func Test_ReadChannel_ChangeType(t *testing.T) {
	// Arrange
	slice := createSlice[int](t)
	ch := channels.FromSlice(slice)
	typeChanger := func(v int) float64 { return float64(v) }
	expected := make([]float64, len(slice))
	for i, v := range slice {
		expected[i] = typeChanger(v)
	}

	// Act
	ch2 := channels.ChangeType(ch, typeChanger)

	// Assert
	assertChannel(t, ch2, expected)
}
