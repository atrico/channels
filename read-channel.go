package channels

type ReadChannel[T any] <-chan T

// Create read channel from a slice
func FromSlice[T any](slice []T) ReadChannel[T] {
	ch := make(chan T)
	// Start write loop
	go func() {
		defer close(ch)
		for _, v := range slice {
			ch <- v
		}
	}()
	return ch
}

func (ch ReadChannel[T]) Select(selector func(v T) T) ReadChannel[T] {
	ch2 := make(chan T, cap(ch))
	go func() {
		defer close(ch2)
		val, ok := <-ch
		for ok {
			ch2 <- selector(val)
			val, ok = <-ch
		}
	}()
	return ch2
}

func (ch ReadChannel[T]) Filter(filter func(v T) bool) ReadChannel[T] {
	ch2 := make(chan T, cap(ch))
	go func() {
		defer close(ch2)
		var val T
		ok := true
		for ok {
			val, ok = <-ch
			if ok && filter(val) {
				ch2 <- val
			}
		}
	}()
	return ch2
}

func ChangeType[Tin, Tout any](ch ReadChannel[Tin], typeChanger func(v Tin) Tout) ReadChannel[Tout] {
	ch2 := make(chan Tout, cap(ch))
	go func() {
		defer close(ch2)
		val, ok := <-ch
		for ok {
			ch2 <- typeChanger(val)
			val, ok = <-ch
		}
	}()
	return ch2
}
